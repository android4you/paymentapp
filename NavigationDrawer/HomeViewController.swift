//
//  HomeViewController.swift
//  NavigationDrawer
//
//  Created by Manu Aravind on 04/02/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {
    
    @IBOutlet weak var shopCircleView: UIImageView!
    @IBOutlet weak var foodCircleView: UIImageView!
    @IBOutlet weak var eventsCircleView: UIImageView!
    
    @IBOutlet weak var shopView: UIView!
    @IBOutlet weak var foodView: UIView!
    @IBOutlet weak var eventsView: UIView!
    
    @IBOutlet weak var widthdrawIV: UIImageView!
    @IBOutlet weak var billsIV: UIImageView!
    @IBOutlet weak var payIV: UIImageView!
    
    @IBOutlet weak var homePayView: UIView!
    @IBOutlet weak var hometopView: UIView!
    
    @IBOutlet weak var profileIconView: UIImageView!
    
    var models = [NewsFeedModel]()
    let cellId = "NewsFeedTableViewCell"
    
    @IBOutlet weak var tableNews: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.init(hex: "EEF6FC")
        hometopView.layer.masksToBounds = true
        hometopView.backgroundColor = UIColor.baseColor()
        hometopView.roundCorners([.bottomLeft, .bottomRight], radius: 20)
        
        let payViewColor : UIColor = UIColor( red: 0.137, green: 0.380, blue:0.490, alpha: 1.0 )
        homePayView.layer.masksToBounds = true
        homePayView.backgroundColor = payViewColor
        homePayView.layer.cornerRadius = 20
        
        payIV.layer.cornerRadius = 12
        billsIV.layer.cornerRadius = 12
        widthdrawIV.layer.cornerRadius = 12
        
        shopView.cardViewByRadius(radius: 12)
        foodView.cardViewByRadius(radius: 12)
        eventsView.cardViewByRadius(radius: 12)
        
        shopCircleView.layer.cornerRadius = 25
        foodCircleView.layer.cornerRadius = 25
        eventsCircleView.layer.cornerRadius = 25
        
        profileIconView.makeRounded()

        tableNews.register(UINib.init(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        tableNews.separatorColor = UIColor.clear
        tableNews.layoutIfNeeded()
        
        let temp = TemplateData()
        models = temp.getNewsFeedData()
        
        typeEvents()
    }
    
    func typeEvents() {
        shopView.isUserInteractionEnabled = true
        foodView.isUserInteractionEnabled = true
        eventsView.isUserInteractionEnabled = true
        let gestureShop = UITapGestureRecognizer(target: self, action:  #selector (self.shopClickEvent))
        shopView.addGestureRecognizer(gestureShop)
        
          let gestureFood = UITapGestureRecognizer(target: self, action:  #selector (self.foodClickEvent))
              foodView.addGestureRecognizer(gestureFood)
        
        let gestureEvent = UITapGestureRecognizer(target: self, action:  #selector (self.eventsClickEvent))
              eventsView.addGestureRecognizer(gestureEvent)
    }
    
    
    @objc func shopClickEvent(_ sender:UITapGestureRecognizer){
          
        }
    
    @objc func foodClickEvent(_ sender:UITapGestureRecognizer){
         slideMenuItemSelectedAtIndex(1)
        }
    
    @objc func eventsClickEvent(_ sender:UITapGestureRecognizer){
          slideMenuItemSelectedAtIndex(2)
        }
    
}

extension HomeViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! NewsFeedTableViewCell
        cell.selectionStyle = .none
        let news = models[indexPath.row]
        cell.titleView.text = news.title
        cell.descriptionView.text = news.description
        cell.imageViewContent.image = UIImage(named: news.image ??  "tour.jpeg")
        DispatchQueue.main.async {
            cell.imageViewContent.roundCorners([.topLeft, .topRight], radius: 20)
            cell.imageViewContent.layer.masksToBounds = true
            cell.containerTextView.roundCorners([.bottomLeft, .bottomRight], radius: 20)
            cell.containerTextView.layer.masksToBounds = true
         }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           var homeVC : NewsViewController!
           homeVC  =  NewsViewController(nibName: "NewsViewController", bundle: nil)
           navigationController?.pushViewController(homeVC, animated: false)
          
    }
}
