//
//  BaseViewController.swift
//  NavigationDrawer
//
//  Created by Manu Aravind on 04/02/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, SlideMenuDelegate {
    let btnShowMenu = UIButton()
    var objMenu : DrawerViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        self.addSlideMenuButton()
        self.addswipeGesture()
    }
    
    func addSlideMenuButton(){
        let navigationBarHeight: CGFloat = self.navigationController!.navigationBar.frame.height
        //btnShowMenu.alpha = 0
        btnShowMenu.setImage(self.defaultMenuImage(), for: UIControl.State())
        btnShowMenu.setImage(self.defaultMenuImage(), for: UIControl.State.highlighted)
        btnShowMenu.frame = CGRect(x: 0, y: 0, width: navigationBarHeight, height: navigationBarHeight)
        btnShowMenu.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControl.Event.touchUpInside)
        let customBarItem = UIBarButtonItem(customView: btnShowMenu)
        self.navigationItem.rightBarButtonItem = customBarItem;
    }
    
    func defaultMenuImage() -> UIImage {
        var defaultMenuImage = UIImage()
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 27, height: 22), false, 0.0)
        UIColor.red.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 3, width: 27, height: 2)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 10, width: 27, height: 2)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 17, width: 27, height: 2)).fill()
        defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return defaultMenuImage;
    }
    
    @objc func onSlideMenuButtonPressed(_ sender : UIButton){
        if (sender.tag == 10) {
            sender.tag = 0
            objMenu.animateWhenViewDisappear()
            return
        }
        
        sender.isEnabled = false
        sender.tag = 10
        objMenu = DrawerViewController(nibName: "DrawerViewController", bundle: nil)
        objMenu.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        objMenu.animateWhenViewAppear()
        objMenu.btnMenu = sender
        objMenu.delegate = self
        UIApplication.shared.keyWindowInConnectedScenes?.addSubview(objMenu.view)
        objMenu.view.layoutIfNeeded()
        sender.isEnabled = true
        
    }
    
    
    func slideMenuItemSelectedAtIndex(_ index: Int32) {
        print(index)
        if (index>=0) {
            
            if(index == 0){
                var homeVC : WalletViewController!
                homeVC  =  WalletViewController(nibName: "WalletViewController", bundle: nil)
                navigationController?.pushViewController(homeVC, animated: false)
                self.navigationController?.navigationItem.title  = "Wallet"

            }else if(index == 1){
                var homeVC : FoodViewController!
                homeVC  =  FoodViewController(nibName: "FoodViewController", bundle: nil)
                navigationController?.pushViewController(homeVC, animated: false)
                          
            }
            
            else if(index == 2){
                var homeVC : EventsViewController!
               homeVC  =  EventsViewController(nibName: "EventsViewController", bundle: nil)
              navigationController?.pushViewController(homeVC, animated: false)
                          
            }
            
            else if(index == 3){
              var homeVC : EventDetailsViewController!
             homeVC  =  EventDetailsViewController(nibName: "EventDetailsViewController", bundle: nil)
           navigationController?.pushViewController(homeVC, animated: false)
                                     
                       }
        }
    }
    
    func addswipeGesture() {
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                print("Swiped right")
               
            case UISwipeGestureRecognizer.Direction.down:
                print("Swiped down")
            case UISwipeGestureRecognizer.Direction.left:
                print("Swiped left")
                 btnShowMenu.sendActions(for: .touchUpInside)
            case UISwipeGestureRecognizer.Direction.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
//    
//    func baseColor() -> UIColor {
//        let yourColor : UIColor = UIColor( red: 0.059, green: 0.169, blue:0.259, alpha: 1.0 )
//        return yourColor
//    }
//    
//    
    
}

