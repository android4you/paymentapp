//
//  UITextField.swift
//  NavigationDrawer
//
//  Created by Manu Aravind on 10/02/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit


extension UITextField {
    func placeholderColor(color: UIColor){
           var placeholderText = ""
           if self.placeholder != nil{
               placeholderText = self.placeholder!
           }
        self.attributedPlaceholder = NSAttributedString(string: placeholderText, attributes: [NSAttributedString.Key.foregroundColor : color])
       }
}
