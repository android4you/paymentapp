//
//  UIView.swift
//  NavigationDrawer
//
//  Created by Manu Aravind on 04/02/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit


extension UIView {
    func cardView(){
        self.backgroundColor = UIColor.white
        //  contentView.backgroundColor = UIColor(red: 240/255.0, green: 240/255.0, blue: 240/255.0, alpha: 1.0)
        self.layer.cornerRadius = 10.0
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.withAlphaComponent(0.4).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowOpacity = 0.7
    }
    
    
    func cardViewByRadius(radius: Int){
        self.backgroundColor = UIColor.white
               //  contentView.backgroundColor = UIColor(red: 240/255.0, green: 240/255.0, blue: 240/255.0, alpha: 1.0)
               self.layer.cornerRadius = CGFloat(radius)
               self.layer.masksToBounds = false
               self.layer.shadowColor = UIColor.black.withAlphaComponent(0.4).cgColor
               self.layer.shadowOffset = CGSize(width: 0, height: 0)
               self.layer.shadowOpacity = 0.4
    }
    
    
    
    
    func cardViewByRadiusBottom(_ corners: UIRectCorner, radius: CGFloat){
        self.backgroundColor = UIColor.white
               //  contentView.backgroundColor = UIColor(red: 240/255.0, green: 240/255.0, blue: 240/255.0, alpha: 1.0)
        
        let maskPath = UIBezierPath(roundedRect: bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: radius, height: radius))
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
       // layer.mask = shape
                  self.layer.mask = shape
               self.layer.shadowColor = UIColor.black.withAlphaComponent(0.9).cgColor
               self.layer.shadowOffset = CGSize(width: 0, height: 0)
               self.layer.shadowOpacity = 0.7
    }

//    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
//            let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
//            let mask = CAShapeLayer()
//            mask.path = path.cgPath
//            self.layer.mask = mask
//       }
    
    public func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: radius, height: radius))
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        layer.mask = shape
    }
    
    
    
    
   public func roundCornerView(cornerRadius: Double) {
        self.layer.cornerRadius = CGFloat(cornerRadius)
        self.clipsToBounds = true
        self.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    self.layer.masksToBounds = false
    self.layer.shadowColor = UIColor.black.withAlphaComponent(0.5).cgColor
             self.layer.shadowOffset = CGSize(width: 0, height: 0)
             self.layer.shadowOpacity = 0.7
    }
    
    
    
    
}
