//
//  UIApplication.swift
//  NavigationDrawer
//
//  Created by Manu Aravind on 04/02/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

extension UIApplication {
    var keyWindowInConnectedScenes: UIWindow? {
          return windows.first(where: { $0.isKeyWindow })
    }
}
