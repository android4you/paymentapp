//
//  UILabel.swift
//  NavigationDrawer
//
//  Created by Manu Aravind on 07/02/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

extension UILabel {

   func makeBold(){
          
           self.font = UIFont.boldSystemFont(ofSize: 16.0)
      
       }

}
