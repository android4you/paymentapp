//
//  UIViewController.swift
//  NavigationDrawer
//
//  Created by Manu Aravind on 04/02/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func setNaviationBar(headline: String){
        self.navigationItem.title = headline
       // self.navigationController?.navigationBar.barTintColor = UIColor(hex: <#T##String#>)
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.font: UIFont(name: "Futura-Medium", size: 22)!,
            NSAttributedString.Key.foregroundColor: UIColor.red
        ]
    }

    
    
    
}
