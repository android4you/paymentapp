//
//  NewsFeedModdel.swift
//  NavigationDrawer
//
//  Created by Manu Aravind on 08/02/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation

class NewsFeedModel {
    var id : Int?
    var title: String?
    var description : String?
    var image : String?
    
    init(id: Int, title: String, description: String, image: String) {
           self.id = id
           self.title = title
           self.description = description
           self.image = image
       }
}
