//
//  FoodOutletsModel.swift
//  NavigationDrawer
//
//  Created by Manu Aravind on 12/02/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation

class FoodOutletsModel {
    var id : Int?
    var title: String?
    var image : String?

    
    
    init(id: Int, title: String,  image: String) {
             self.id = id
             self.title = title
             self.image = image
         }
}
