//
//  TemplateData.swift
//  NavigationDrawer
//
//  Created by Manu Aravind on 08/02/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import Foundation


class TemplateData{
    
    func getNewsFeedData()  -> [NewsFeedModel] {
        var models = [NewsFeedModel]()
        models.append(NewsFeedModel(id: 1, title: "Athirapally Falls", description: "Athirappilly Falls, is situated in Athirappilly Panchayat in Chalakudy Taluk of Thrissur District in Kerala, India on the Chalakudy River, which originates from the upper reaches of the Western Ghats at the entrance to the Sholayar ranges. It is the largest waterfall in Kerala, which stands tall at 80 feet.", image: "falls.jpg"))
        
        models.append(NewsFeedModel(id: 2, title: "Vagamon", description: "Vagamon, also spelt Wagamon, is an Indian hill station town primarily located in Peerumade taluk of Idukki district, and also Meenachil taluk and Kanjirappally taluk of Kottayam district in the state of Kerala.", image: "tour.jpeg"))
        
        models.append(NewsFeedModel(id: 3, title: "Alappuzha", description: "Alappuzha (or Alleppey) is a city on the Laccadive Sea in the southern Indian state of Kerala. It's best known for houseboat cruises along the rustic Kerala backwaters, a network of tranquil canals and lagoons. Alappuzha Beach is the site of the 19th-century Alappuzha Lighthouse. ", image: "alappuzha.jpg"))
        return models
    }
    
    
    func  menuItems() -> [MenuModel] {
        var models = [MenuModel]()
        models.append(MenuModel(id: 1, title: "Access Wallet", description: "Manage your Going Wallet", image: "wallet.png"))
        models.append(MenuModel(id: 2, title: "Notifications", description: "Follow up on orders and important updates", image: "notification.png"))
        models.append(MenuModel(id: 3, title: "Profile Settings", description: "Update Photo Info, Change Photo..", image: "profile"))
        models.append(MenuModel(id: 4, title: "Manage Addresses", description: "Update delivery address", image: "map.png"))
        models.append(MenuModel(id: 5, title: "Support", description: "FAQs, Contact support team", image: "support.png"))
        return models
    }
    
    
    
    
    func  eventItems() -> [EventsModel] {
        var models = [EventsModel]()
        models.append(EventsModel(id: 1, title: "Access Wallet", description: "Manage your Going Wallet", image: "eb.png"))
        models.append(EventsModel(id: 2, title: "Notifications", description: "Follow up on orders and important updates", image: "football.png"))
        models.append(EventsModel(id: 3, title: "Profile Settings", description: "Update Photo Info, Change Photo..", image: "cricket.png"))

        return models
    }
    
    
    func  foodItems() -> [FoodOutletsModel] {
           var models = [FoodOutletsModel]()
           models.append(FoodOutletsModel(id: 1, title: "Kfc",  image: "kfc.png"))
           models.append(FoodOutletsModel(id: 2, title: "McDonalds", image: "mcd.png"))
           models.append(FoodOutletsModel(id: 3, title: "Subway", image: "suway.png"))
           models.append(FoodOutletsModel(id: 4, title: "Hungry Lion", image: "hungrylion.png"))
        models.append(FoodOutletsModel(id: 1, title: "Kfc",  image: "kfc.png"))
              models.append(FoodOutletsModel(id: 2, title: "McDonalds", image: "mcd.png"))
              models.append(FoodOutletsModel(id: 3, title: "Subway", image: "suway.png"))
              models.append(FoodOutletsModel(id: 4, title: "Hungry Lion", image: "hungrylion.png"))
        models.append(FoodOutletsModel(id: 1, title: "Kfc",  image: "kfc.png"))
              models.append(FoodOutletsModel(id: 2, title: "McDonalds", image: "mcd.png"))
              models.append(FoodOutletsModel(id: 3, title: "Subway", image: "suway.png"))
              models.append(FoodOutletsModel(id: 4, title: "Hungry Lion", image: "hungrylion.png"))
           return models
       }
    
}
