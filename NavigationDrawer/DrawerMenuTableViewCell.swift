//
//  DrawerMenuTableViewCell.swift
//  NavigationDrawer
//
//  Created by Manu Aravind on 07/02/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class DrawerMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var descriptionView: UILabel!
    @IBOutlet weak var containerCard: UIView!
    @IBOutlet weak var menuTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerCard.cardView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
