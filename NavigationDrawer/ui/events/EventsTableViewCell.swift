//
//  EventsTableViewCell.swift
//  NavigationDrawer
//
//  Created by Manu Aravind on 09/02/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class EventsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageTitleView: UIImageView!
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var descriptionView: UILabel!
    @IBOutlet weak var bottomView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleView.textColor = UIColor.baseColor()
        descriptionView.textColor = UIColor.baseColor()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
