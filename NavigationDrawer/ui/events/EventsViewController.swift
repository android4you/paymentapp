//
//  EventsViewController.swift
//  NavigationDrawer
//
//  Created by Manu Aravind on 08/02/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class EventsViewController: UIViewController {

    @IBOutlet weak var backView: UIImageView!
    @IBOutlet weak var myTicketView: UIView!
    @IBOutlet weak var topContainerView: UIView!
    @IBOutlet weak var bottomContainerView: UIView!
    
    @IBOutlet weak var searchItemView: UIView!
    @IBOutlet weak var eventsDropDown: UIView!
    @IBOutlet weak var searchContainerrView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    let cellId = "EventsTableViewCell"
    var models = [EventsModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.baseColor()
        topContainerView.backgroundColor = UIColor.baseColor()
        bottomContainerView.roundCorners([.topLeft, .topRight], radius: 30)
        myTicketView.layer.cornerRadius = 20
        backbutton()
        
        let payViewColor : UIColor = UIColor( red: 0.137, green: 0.380, blue:0.490, alpha: 1.0 )
        searchContainerrView.layer.masksToBounds = true
        searchContainerrView.backgroundColor = payViewColor
        searchContainerrView.layer.cornerRadius = 15
        
        searchItemView.layer.masksToBounds = true
        searchItemView.backgroundColor = payViewColor
        searchItemView.layer.cornerRadius = 15
        
        eventsDropDown.layer.masksToBounds = true
        eventsDropDown.backgroundColor = payViewColor
        eventsDropDown.layer.cornerRadius = 15
        
        tableView.register(UINib.init(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        tableView.separatorColor = UIColor.clear
        let temp = TemplateData()
        models = temp.eventItems()
        tableView.reloadData()
    }

    func backbutton(){
          backView.image = backView.image?.withRenderingMode(.alwaysTemplate)
          backView.tintColor = UIColor.white
          backView.isUserInteractionEnabled = true
          let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.someAction (_:)))
          backView.addGestureRecognizer(gesture)
      }
    
    @objc func someAction(_ sender:UITapGestureRecognizer){
         self.navigationController?.popViewController(animated: false)
       }
}

extension EventsViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! EventsTableViewCell
        cell.selectionStyle = .none
        let product = models[indexPath.row]
        cell.titleView.text = product.title
        cell.imageTitleView.image = UIImage(named: product.image ??  "tour.jpeg")
            DispatchQueue.main.async {
                cell.imageTitleView.roundCorners([.topLeft, .topRight], radius: 20)
                cell.imageTitleView.layer.masksToBounds = true
                cell.bottomView.roundCornerView(cornerRadius: 20.0)
             }
                
        return cell
    
    }
}



