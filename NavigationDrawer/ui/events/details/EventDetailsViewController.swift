//
//  EventDetailsViewController.swift
//  NavigationDrawer
//
//  Created by Manu Aravind on 08/02/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class EventDetailsViewController: UIViewController {

    
    @IBOutlet weak var bottomContainerView: UIView!
    @IBOutlet weak var backView: UIImageView!
    @IBOutlet weak var buyTicketButton: UIButton!
    @IBOutlet weak var standardView: UIView!
    @IBOutlet weak var vipView: UIView!
    @IBOutlet weak var standardIncrementBtn: UIButton!
    @IBOutlet weak var standardDecrementButton: UIButton!
    @IBOutlet weak var vipIncrementBtn: UIButton!
    @IBOutlet weak var vipDecrementBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomContainerView.roundCorners([.topLeft, .topRight], radius: 30)
        
        standardView.layer.cornerRadius = 10
        vipView.layer.cornerRadius = 10
        buyTicketButton.layer.cornerRadius = 15
        
        standardIncrementBtn.layer.cornerRadius = 15
        standardDecrementButton.layer.cornerRadius = 15
        vipIncrementBtn.layer.cornerRadius = 15
        vipDecrementBtn.layer.cornerRadius = 15
        backbutton()
    }

    func backbutton(){
          backView.image = backView.image?.withRenderingMode(.alwaysTemplate)
          backView.tintColor = UIColor.white
          backView.isUserInteractionEnabled = true
          let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.someAction (_:)))
          backView.addGestureRecognizer(gesture)
      }
    
    @objc func someAction(_ sender:UITapGestureRecognizer){
         self.navigationController?.popViewController(animated: false)
       }


}
