//
//  WalletViewController.swift
//  NavigationDrawer
//
//  Created by Manu Aravind on 08/02/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class WalletViewController: UIViewController {

    @IBOutlet weak var walletTopView: UIView!
    @IBOutlet weak var walletBalanceView: UIView!
    @IBOutlet weak var backView: UIImageView!
    @IBOutlet weak var transactionlistView: UIView!
    @IBOutlet weak var addmoneyButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.baseColor()
        walletTopView.backgroundColor = UIColor.baseColor()
        walletBalanceView.layer.cornerRadius = 20
        transactionlistView.roundCorners([.topLeft, .topRight], radius: 20)
        addmoneyButton.layer.cornerRadius = 10
        backbutton()
    }
    
    func backbutton(){
        backView.image = backView.image?.withRenderingMode(.alwaysTemplate)
        backView.tintColor = UIColor.white
        backView.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.someAction (_:)))
        backView.addGestureRecognizer(gesture)
    }
    
    @objc func someAction(_ sender:UITapGestureRecognizer){
      self.navigationController?.popViewController(animated: false)
    }

}
