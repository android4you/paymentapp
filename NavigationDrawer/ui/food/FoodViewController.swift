//
//  FoodViewController.swift
//  NavigationDrawer
//
//  Created by Manu Aravind on 08/02/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class FoodViewController: UIViewController {
    
    let cellId = "PopularCollectionViewCell"
    let cellId2 = "RestaurantsCollectionViewCell"
    @IBOutlet weak var backView: UIImageView!
    @IBOutlet weak var searchLocationView: UIView!
    @IBOutlet weak var searchFoodItemView: UIView!
    @IBOutlet weak var containerBottomView: UIView!
    @IBOutlet weak var topContainerView: UIView!
    @IBOutlet weak var searchLocationField: UITextField!
    @IBOutlet weak var searchFoodItemField: UITextField!
    @IBOutlet weak var popularContainerView: UIView!
    
    @IBOutlet weak var popularCollectionView: UICollectionView!
    
    @IBOutlet weak var restaurantsCollectionView: UICollectionView!
    
     var models = [FoodOutletsModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.baseColor()
        topContainerView.backgroundColor = UIColor.baseColor()
        let payViewColor : UIColor = UIColor( red: 0.137, green: 0.380, blue:0.490, alpha: 1.0 )
        searchLocationView.layer.masksToBounds = true
        searchLocationView.backgroundColor = payViewColor
        searchLocationView.layer.cornerRadius = 15
        
        searchFoodItemView.layer.masksToBounds = true
        searchFoodItemView.backgroundColor = payViewColor
        searchFoodItemView.layer.cornerRadius = 15
        
        searchLocationField.placeholderColor(color: UIColor.white)
        searchFoodItemField.placeholderColor(color: UIColor.white)
        containerBottomView.roundCorners([.topLeft, .topRight], radius: 30)
        
        backbutton()
        popularCollectionView.register(UINib.init(nibName: "PopularCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PopularCollectionViewCell")
        
        let temp = TemplateData()
              models = temp.foodItems()
        
        
        restaurantsCollectionView.register(UINib.init(nibName: "RestaurantsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "RestaurantsCollectionViewCell")
        
    }

    func backbutton(){
          backView.image = backView.image?.withRenderingMode(.alwaysTemplate)
          backView.tintColor = UIColor.white
          backView.isUserInteractionEnabled = true
          let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.someAction (_:)))
          backView.addGestureRecognizer(gesture)
      }
    
    @objc func someAction(_ sender:UITapGestureRecognizer){
         self.navigationController?.popViewController(animated: false)
    }
}


extension FoodViewController: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.popularCollectionView {
            return models.count
        } else {
            return models.count
        }
     }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.popularCollectionView {
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PopularCollectionViewCell
             let product = models[indexPath.row]
             cell.imageView.image = UIImage(named: product.image ??  "tour.jpeg")
             cell.titleView.text = product.title
             return cell
         }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId2, for: indexPath) as! RestaurantsCollectionViewCell
            cell.logoView.makeRounded10()
            return cell
        }
    }
}

extension FoodViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row + 1)
    }
}

extension FoodViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
          if collectionView == self.popularCollectionView {
            return CGSize(width: collectionView.bounds.size.width/4, height: 150)
        }

        return CGSize(width: collectionView.bounds.size.width/3, height: 140)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
         return 0
     }
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
         return 0
     }

    }
