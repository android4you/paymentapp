//
//  NewsViewController.swift
//  NavigationDrawer
//
//  Created by Manu Aravind on 13/02/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class NewsViewController: UIViewController {
    
    @IBOutlet weak var bacView: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var topView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.baseColor()
        topView.backgroundColor = UIColor.baseColor()
        bottomView.roundCorners([.topLeft], radius: 30)
        backbutton()
    }

    func backbutton(){
        bacView.image = bacView.image?.withRenderingMode(.alwaysTemplate)
        bacView.tintColor = UIColor.white
        bacView.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.someAction (_:)))
        bacView.addGestureRecognizer(gesture)
    }
    
    @objc func someAction(_ sender:UITapGestureRecognizer){
            self.navigationController?.popViewController(animated: false)
    }
}
