//
//  NewsFeedTableViewCell.swift
//  NavigationDrawer
//
//  Created by Manu Aravind on 07/02/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit

class NewsFeedTableViewCell: UITableViewCell {

    @IBOutlet weak var containerTextView: UIView!
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var imageViewContent: UIImageView!
    
    @IBOutlet weak var descriptionView: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        titleView.textColor = UIColor.baseColor()
        descriptionView.textColor = UIColor.baseColor()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        

        // Configure the view for the selected state
    }
    
}
