//
//  DrawerViewController.swift
//  NavigationDrawer
//
//  Created by Manu Aravind on 04/02/2020.
//  Copyright © 2020 Manu Aravind. All rights reserved.
//

import UIKit


protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32)
}

class DrawerViewController: UIViewController {
    
    var delegate : SlideMenuDelegate?
    var btnMenu : UIButton!
    var models = [MenuModel]()
    let cellId = "DrawerMenuTableViewCell"
    
    @IBOutlet weak var tableMenu: UITableView!
    @IBOutlet weak var drawerBaseView: UIView!
    @IBOutlet weak var btnCloseMenu: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .clear
        UINavigationBar.appearance().barTintColor = UIColor(named: "4c1d12")
        UINavigationBar.appearance().tintColor = UIColor.white
        self.view.frame = CGRect(x: self.view.bounds.size.width*2, y: 0, width: self.view.bounds.size.width,height: self.view.bounds.size.height)
        self.view.layoutIfNeeded()
        self.view.backgroundColor = UIColor.clear
        self.view.insetsLayoutMarginsFromSafeArea = false
        self.btnCloseMenu.alpha = 0.0
        self.btnCloseMenu.backgroundColor = UIColor.darkGray.withAlphaComponent(0.0)
        
        drawerBaseView.layer.cornerRadius = 25.0
        
        profileImageView.makeRounded()
        
        tableMenu.register(UINib.init(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        tableMenu.separatorColor = UIColor.clear
        tableMenu.layoutIfNeeded()
        
     let temp = TemplateData()
        models = temp.menuItems()
        tableMenu.reloadData()
    }

    func animateWhenViewAppear(){
         self.view.backgroundColor = .clear
        self.btnCloseMenu.alpha = 0.0
        self.btnCloseMenu.backgroundColor = UIColor.darkGray.withAlphaComponent(0.0)
        self.view.backgroundColor = UIColor.clear
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width,height: self.view.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = .clear
        }, completion: { (finished) -> Void in
            self.btnCloseMenu.backgroundColor = UIColor.darkGray.withAlphaComponent(0.4)
            self.btnCloseMenu.alpha = 0.3
            self.view.backgroundColor = .clear
        })
    }
    
    func animateWhenViewDisappear(){
        self.btnCloseMenu.alpha = 0.0
        self.view.backgroundColor = UIColor.clear
        self.btnCloseMenu.backgroundColor = UIColor.darkGray.withAlphaComponent(0.0)
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.view.frame = CGRect(x: self.view.bounds.size.width*2, y: 0, width: self.view.bounds.size.width,height: self.view.bounds.size.height)
            self.view.layoutIfNeeded()
            }, completion: { (finished) -> Void in
            self.willMove(toParent: nil)
            self.view.removeFromSuperview()
            self.removeFromParent()
            self.btnMenu.tag = 0
            self.view.backgroundColor = UIColor.clear
            self.btnCloseMenu.alpha = 0.0
            self.btnCloseMenu.backgroundColor = UIColor.darkGray.withAlphaComponent(0.0)
        })
    }
    
    @IBAction func sidemenuCloseClick(_ sender: Any) {
           animateWhenViewDisappear()
    }
}


extension DrawerViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! DrawerMenuTableViewCell
        cell.selectionStyle = .none
        let menu = models[indexPath.row]
        cell.menuTitle.text = menu.title
        cell.descriptionView.text = menu.description
        cell.iconView.image = UIImage(named: menu.image ??  "profile.png")
        return cell
    
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          /// print("You selected cell number: \(indexPath.row)!")
           animateWhenViewDisappear()
           
           if (self.delegate != nil) {
               
               self.delegate?.slideMenuItemSelectedAtIndex(Int32(indexPath.row))
           }
           
           
       }
}
